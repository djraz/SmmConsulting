package com.smmconsulting.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.smmconsulting.entities.Formation;

public interface FormationRepository extends JpaRepository<Formation, Long> {

}
