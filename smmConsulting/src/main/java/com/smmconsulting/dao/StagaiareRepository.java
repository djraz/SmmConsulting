package com.smmconsulting.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.smmconsulting.entities.Stagiaire;

public interface StagaiareRepository extends JpaRepository<Stagiaire, Long> {

}
