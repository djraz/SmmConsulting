package com.smmconsulting;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.smmconsulting.dao.FormationRepository;
import com.smmconsulting.entities.Formation;

@RestController
public class FormationWebService {
	
	@Autowired
	private FormationRepository formatonRepository;
	
	@RequestMapping(value="/formations",method=RequestMethod.GET)
	public List<Formation> getAllFormation(){
		return formatonRepository.findAll();
	}

}
