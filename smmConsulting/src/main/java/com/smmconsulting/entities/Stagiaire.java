package com.smmconsulting.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the stagiaire database table.
 * 
 */
@Entity
@NamedQuery(name="Stagiaire.findAll", query="SELECT s FROM Stagiaire s")
public class Stagiaire implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id_stagiaire")
	private long idStagiaire;

	private String adresse;

	@Column(name="anne_naissance")
	private int anneNaissance;

	private String email;

	private String nom;

	private String prenom;

	
	private long telephone;

	//bi-directional many-to-many association to Formation
	@ManyToMany
	@JoinTable(
		name="s_inscrire"
		, joinColumns={
			@JoinColumn(name="id_stagiaire")
			}
		, inverseJoinColumns={
			@JoinColumn(name="id_formations")
			}
		)
	private List<Formation> formations;

	public Stagiaire() {
	}

	public long getIdStagiaire() {
		return this.idStagiaire;
	}

	public void setIdStagiaire(long idStagiaire) {
		this.idStagiaire = idStagiaire;
	}

	public String getAdresse() {
		return this.adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public int getAnneNaissance() {
		return this.anneNaissance;
	}

	public void setAnneNaissance(int anneNaissance) {
		this.anneNaissance = anneNaissance;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return this.prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public long getTelephone() {
		return this.telephone;
	}

	public void setTelephone(long telephone) {
		this.telephone = telephone;
	}

	public List<Formation> getFormations() {
		return this.formations;
	}

	public void setFormations(List<Formation> formations) {
		this.formations = formations;
	}

	public Stagiaire(String adresse, int anneNaissance, String email, String nom, String prenom) {
		super();
		this.adresse = adresse;
		this.anneNaissance = anneNaissance;
		this.email = email;
		this.nom = nom;
		this.prenom = prenom;
		
	}

	public Stagiaire(String adresse, int anneNaissance, String email, String nom, String prenom, long telephone) {
		super();
		this.adresse = adresse;
		this.anneNaissance = anneNaissance;
		this.email = email;
		this.nom = nom;
		this.prenom = prenom;
		this.telephone = telephone;
	}

	
	
	

}