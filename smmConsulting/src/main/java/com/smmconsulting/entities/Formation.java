package com.smmconsulting.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the formations database table.
 * 
 */
@Entity
@Table(name="formations")
@NamedQuery(name="Formation.findAll", query="SELECT f FROM Formation f")
public class Formation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id_formations")
	private int idFormations;

	private String description;

	private String intitule;

	//bi-directional many-to-many association to Cour
	@ManyToMany
	@JoinTable(
		name="contient"
		, joinColumns={
			@JoinColumn(name="id_formations")
			}
		, inverseJoinColumns={
			@JoinColumn(name="id_cours")
			}
		)
	private List<Cour> cours;

	//bi-directional many-to-one association to Session
	@ManyToOne
	@JoinColumn(name="id")
	private Session session;

	//bi-directional many-to-many association to Stagiaire
	@ManyToMany(mappedBy="formations")
	private List<Stagiaire> stagiaires;

	public Formation() {
	}

	public int getIdFormations() {
		return this.idFormations;
	}

	public void setIdFormations(int idFormations) {
		this.idFormations = idFormations;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIntitule() {
		return this.intitule;
	}

	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}

	public List<Cour> getCours() {
		return this.cours;
	}

	public void setCours(List<Cour> cours) {
		this.cours = cours;
	}

	public Session getSession() {
		return this.session;
	}

	public void setSession(Session session) {
		this.session = session;
	}

	public List<Stagiaire> getStagiaires() {
		return this.stagiaires;
	}

	public void setStagiaires(List<Stagiaire> stagiaires) {
		this.stagiaires = stagiaires;
	}

	public Formation(String description, String intitule) {
		super();
		this.description = description;
		this.intitule = intitule;
	}
	
	

}