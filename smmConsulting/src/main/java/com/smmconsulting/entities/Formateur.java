package com.smmconsulting.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the formateurs database table.
 * 
 */
@Entity
@Table(name="formateurs")
@NamedQuery(name="Formateur.findAll", query="SELECT f FROM Formateur f")
public class Formateur implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id_formateurs")
	private int idFormateurs;

	private String email;

	private String nom;

	private String prenom;

	@Lob
	private byte[] telephone;

	//bi-directional many-to-many association to Cour
	@ManyToMany
	@JoinTable(
		name="relation3"
		, joinColumns={
			@JoinColumn(name="id_formateurs")
			}
		, inverseJoinColumns={
			@JoinColumn(name="id_cours")
			}
		)
	private List<Cour> cours;

	public Formateur() {
	}

	public int getIdFormateurs() {
		return this.idFormateurs;
	}

	public void setIdFormateurs(int idFormateurs) {
		this.idFormateurs = idFormateurs;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return this.prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public byte[] getTelephone() {
		return this.telephone;
	}

	public void setTelephone(byte[] telephone) {
		this.telephone = telephone;
	}

	public List<Cour> getCours() {
		return this.cours;
	}

	public void setCours(List<Cour> cours) {
		this.cours = cours;
	}

}