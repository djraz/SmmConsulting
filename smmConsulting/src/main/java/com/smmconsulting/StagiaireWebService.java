package com.smmconsulting;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.smmconsulting.dao.StagaiareRepository;
import com.smmconsulting.entities.Stagiaire;

@RestController
public class StagiaireWebService {
	
	@Autowired
	private StagaiareRepository  stagaiareRepository;
	
	@RequestMapping(value="/stagiaires",method=RequestMethod.GET)
	public List<Stagiaire> getStagiaires(){
		return stagaiareRepository.findAll();
	}
	
	@RequestMapping(value="/stagiaires/{id}",method=RequestMethod.GET)
	public Stagiaire getOneStagiaire(@PathVariable long id){
		return stagaiareRepository.findOne(id);
	}
	
	@RequestMapping(value="/stagiaires",method=RequestMethod.POST)
	public void ajouterStagiaires(@RequestBody Stagiaire stg){
		stagaiareRepository.save(stg);
	}
	
	
	@RequestMapping(value="/stagiaires/{id}",method=RequestMethod.DELETE)
	public boolean supprimer(@PathVariable long id){
	 stagaiareRepository.delete(id);;
	 return true;
	}

}
