package com.smmconsulting;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.smmconsulting.dao.FormationRepository;
import com.smmconsulting.dao.StagaiareRepository;
import com.smmconsulting.entities.Formation;
import com.smmconsulting.entities.Stagiaire;



@SpringBootApplication
public class SmmConsultingApplication implements CommandLineRunner{
   
	@Autowired
	private StagaiareRepository  stagaiareRepository;
	
	@Autowired
	private FormationRepository formationRepository;
	
	
	public static void main(String[] args) {
		SpringApplication.run(SmmConsultingApplication.class, args);
	}

	@Override
	public void run(String... arg0) throws Exception {
		
		stagaiareRepository.save(new Stagiaire("1 avenue par",1990,"toto@gmail.com","toto","titi",06521432141));
		stagaiareRepository.save(new Stagiaire("1 avenue par",1990,"papa@gmail.com","papa","titi",06521432141));
		stagaiareRepository.save(new Stagiaire("1 avenue par",1990,"kia@gmail.com","kia","titi",06521432141));
		stagaiareRepository.save(new Stagiaire("1 avenue par",1990,"mounir@gmail.com","mounir","titi",06521432141));
		stagaiareRepository.save(new Stagiaire("1 avenue par",1990,"loolo@gmail.com","loolo","titi",06521432141));
		stagaiareRepository.save(new Stagiaire("1 avenue par",1990,"pato@gmail.com","pato","titi",06521432141));
		stagaiareRepository.save(new Stagiaire("1 avenue par",1990,"koko@gmail.com","koko","titi",06521432141));
		stagaiareRepository.save(new Stagiaire("1 avenue par",1990,"mylo@gmail.com","mylo","titi",06521432141));
		stagaiareRepository.save(new Stagiaire("1 avenue par",1990,"dj@gmail.com","dj","titi",06521432141));
		stagaiareRepository.save(new Stagiaire("1 avenue par",1990,"moussa@gmail.com","moussa","titi",06521432141));
		stagaiareRepository.save(new Stagiaire("1 avenue par",1990,"solo@gmail.com","solo","titi",06521432141));
		
		
		stagaiareRepository.findAll().forEach(c->{
			System.out.println(c.getNom());
		});
		
		formationRepository.save(new Formation("initiation java jee","JAVA JEE"));
		System.out.println(formationRepository.findAll());
		
	}
}
